import deepitope as dp
import parsedata
import random
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import sys
import itertools
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import sequence
matplotlib.use('Agg')

STANDARD_AAS = ['A','G','I','L','P','V','F','W','Y','D','E','R','H','K','S','T','C','M','N','Q']


def get_bin_index(val, _bins):
    bin_index = 0
    for _bin in _bins:
        if val < _bin:
            return bin_index - 1
        bin_index = bin_index + 1
    return bin_index - 1


def get_p_value(_score, _all_scores):
    p = 0
    for s in _all_scores:
        if _score > s:
            p = p + 1
    return p/len(_all_scores)


def shuffle_word(word, seed):
    word = list(word)
    random.Random(seed).shuffle(word)
    return ''.join(word)


def get_random_sequences(pred):
    _random_list = []
    for i in range(15000):
        _random_list.append(shuffle_word(pred, i))
    _random_list = list(set(_random_list))    # getting unique values
    if pred in _random_list:
        _random_list.remove(pred)
    _random_list = _random_list[:1000]    # getting only 1000 values
    if len(_random_list) != 1000:
        _random_list = []
        perm = list(itertools.permutations(pred))
        perm = perm[1:1001]  # getting only 1000 values
        for word in perm:
            _random_list.append(''.join(word))
    return sorted(_random_list)


def seq_to_string(sequence_list):
    sequences_string = ''
    for sequence in sequence_list:
        sequences_string = sequences_string + sequence
    return sequences_string


def predict_scores(model, randomlist):
    ''' Searches 'model' dir for trained models of all
        sizes and returns a tuple with the resulting
        preds: (score, peptide, start_pos, end_pos) '''

    sizes = [sizedir for sizedir in os.listdir(model)
             if os.path.isdir(os.path.join(model, sizedir))]

    for size in sizes:

        modeldir = os.path.join(model, size)
        model = load_model(modeldir)

        indexed_list = parsedata.index_peptide_list(randomlist)

        x_test = parsedata.translate(indexed_list, True)
        x_test = sequence.pad_sequences(x_test)

        # convert to numpy
        x_test = np.asarray(x_test)
        x_test = x_test.astype(float)

        # uses trained model to predict
        y_score = model.predict(x_test)

        # forms a list of tuples with all predictions:
        results = []
        for seq, score, pos in zip(randomlist, y_score, range(0, len(randomlist))):
            results += [(round(score[0], 3), seq, pos, pos + int(size) - 1)]

    return sorted(results)


# Getting predictions from fasta file
pred_mhci = np.array(dp.predict('mhci', 'SpikeWT2.fasta'))
pred_mhcii = np.array(dp.predict('mhcii', 'SpikeWT2.fasta'))

if not os.path.exists('tests/tops/'):
    os.makedirs('tests/tops/')

# Write everything to file
sys.stdout = open("tests/tops/results.txt", "w+")


# Getting best, worst and middle scored MHCI sequences
best_10_mhci = pred_mhci[:10]
index = int(round(len(pred_mhci) / 2)) - 5
middle_10_mhci = pred_mhci[index:index + 10]
worst_10_mhci = pred_mhci[-10:]


# Plotting histogram for MHCI scores
n, bins, patches = plt.hist(np.asarray(pred_mhci[:, 0], dtype='float'), bins=np.arange(0, 1, 0.01))
for value in np.asarray(best_10_mhci[:, 0], dtype='float'):
    i = get_bin_index(value, bins)
    patches[i].set_fc('r')
for value in np.asarray(middle_10_mhci[:, 0], dtype='float'):
    i = get_bin_index(value, bins)
    patches[i].set_fc('r')
for value in np.asarray(worst_10_mhci[:, 0], dtype='float'):
    i = get_bin_index(value, bins)
    patches[i].set_fc('r')
plt.title("MHCI Histogram")
plt.xticks(np.arange(0, 1.1, 0.1))
plt.savefig('tests/tops/mhci_hist.png', bbox_inches='tight')
plt.clf()


# Getting best, worst and middle scored MHCII sequences
best_10_mhcii = pred_mhcii[:10]
index = int(round(len(pred_mhcii) / 2)) - 5
middle_10_mhcii = pred_mhcii[index:index + 10]
worst_10_mhcii = pred_mhcii[-10:]


# Plotting histogram for MHCII scores
n, bins, patches = plt.hist(np.asarray(pred_mhcii[:, 0], dtype='float'), bins=np.arange(0, 1, 0.01))
for value in np.asarray(best_10_mhcii[:, 0], dtype='float'):
    i = get_bin_index(value, bins)
    patches[i].set_fc('r')
for value in np.asarray(middle_10_mhcii[:, 0], dtype='float'):
    i = get_bin_index(value, bins)
    patches[i].set_fc('r')
for value in np.asarray(worst_10_mhcii[:, 0], dtype='float'):
    i = get_bin_index(value, bins)
    patches[i].set_fc('r')
plt.title("MHCII Histogram")
plt.xticks(np.arange(0, 1.1, 0.1))
plt.savefig('tests/tops/mhcii_hist.png', bbox_inches='tight')
plt.clf()


# Getting p values for MHCI scores
all_scores_mhci = np.asarray(pred_mhci[:, 0], dtype='float')
best_scores_mhci = np.asarray(best_10_mhci[:, 0], dtype='float')
middle_scores_mhci = np.asarray(middle_10_mhci[:, 0], dtype='float')
worst_scores_mhci = np.asarray(worst_10_mhci[:, 0], dtype='float')

p_values_mhci = []
for score in best_scores_mhci:
    p_values_mhci.append(get_p_value(score, all_scores_mhci))
for score in middle_scores_mhci:
    p_values_mhci.append(get_p_value(score, all_scores_mhci))
for score in worst_scores_mhci:
    p_values_mhci.append(get_p_value(score, all_scores_mhci))

scores_mhci = [best_scores_mhci, middle_scores_mhci, worst_scores_mhci]
sequences_mhci = [best_10_mhci[:, 1], middle_10_mhci[:, 1], worst_10_mhci[:, 1]]

scores_mhci = np.array(scores_mhci).flatten()
sequences_mhci = np.array(sequences_mhci).flatten()
p_values_mhci = np.array(p_values_mhci).flatten()

tabla_mhci = np.vstack((scores_mhci, sequences_mhci, p_values_mhci)).T

df_mhci = pd.DataFrame(data=tabla_mhci, columns=["score", "sequence", "p_value"])
print("------------- MHCI ------------")
print(df_mhci)
print("\n")

# Getting p values for MHCII scores
all_scores_mhcii = np.asarray(pred_mhcii[:, 0], dtype='float')
best_scores_mhcii = np.asarray(best_10_mhcii[:, 0], dtype='float')
middle_scores_mhcii = np.asarray(middle_10_mhcii[:, 0], dtype='float')
worst_scores_mhcii = np.asarray(worst_10_mhcii[:, 0], dtype='float')

p_values_mhcii = []
for score in best_scores_mhcii:
    p_values_mhcii.append(get_p_value(score, all_scores_mhcii))
for score in middle_scores_mhcii:
    p_values_mhcii.append(get_p_value(score, all_scores_mhcii))
for score in worst_scores_mhcii:
    p_values_mhcii.append(get_p_value(score, all_scores_mhcii))

scores_mhcii = [best_scores_mhcii, middle_scores_mhcii, worst_scores_mhcii]
sequences_mhcii = [best_10_mhcii[:, 1], middle_10_mhcii[:, 1], worst_10_mhcii[:, 1]]

scores_mhcii = np.array(scores_mhcii).flatten()
sequences_mhcii = np.array(sequences_mhcii).flatten()
p_values_mhcii = np.array(p_values_mhcii).flatten()

tabla_mhcii = np.vstack((scores_mhcii, sequences_mhcii, p_values_mhcii)).T

df_mhcii = pd.DataFrame(data=tabla_mhcii, columns=["score", "sequence", "p_value"])
print("------------ MHCII ------------")
print(df_mhcii)
print("\n")

print("------------- MHCI ------------")
print("-------------------------------")
print("-------- 10 Low Scored --------")
print("-------------------------------")
print("\n")

for i in range(len(best_10_mhci)):
    random_list_mhci = get_random_sequences(best_10_mhci[i][1])
    results_mhci = np.array(predict_scores('mhci', random_list_mhci))
    print("original sequence: ", best_10_mhci[i])
    print("most immunogenic: ", results_mhci[0])
    print("least immunogenic: ", results_mhci[-1])
    print("score avg: ", np.mean(np.asarray(results_mhci[:, 0], dtype='float')))
    print("score std: ", np.std(np.asarray(results_mhci[:, 0], dtype='float')))
    print("p value: ", get_p_value(float(best_10_mhci[i][0]), np.asarray(results_mhci[:, 0], dtype='float')))
    print("-------------------------------")

print("\n")
print("-------------------------------")
print("------ 10 Middle Scored -------")
print("-------------------------------")
print("\n")

for i in range(len(middle_10_mhci)):
    random_list_mhci = get_random_sequences(middle_10_mhci[i][1])
    results_mhci = np.array(predict_scores('mhci', random_list_mhci))
    print("original sequence: ", middle_10_mhci[i])
    print("most immunogenic: ", results_mhci[0])
    print("least immunogenic: ", results_mhci[-1])
    print("score avg: ", np.mean(np.asarray(results_mhci[:, 0], dtype='float')))
    print("score std: ", np.std(np.asarray(results_mhci[:, 0], dtype='float')))
    print("p value: ", get_p_value(float(middle_10_mhci[i][0]), np.asarray(results_mhci[:, 0], dtype='float')))
    print("-------------------------------")

print("\n")
print("-------------------------------")
print("-------- 10 High Scored -------")
print("-------------------------------")
print("\n")

for i in range(len(worst_10_mhci)):
    random_list_mhci = get_random_sequences(worst_10_mhci[i][1])
    results_mhci = np.array(predict_scores('mhci', random_list_mhci))
    print("original sequence: ", worst_10_mhci[i])
    print("most immunogenic: ", results_mhci[0])
    print("least immunogenic: ", results_mhci[-1])
    print("score avg: ", np.mean(np.asarray(results_mhci[:, 0], dtype='float')))
    print("score std: ", np.std(np.asarray(results_mhci[:, 0], dtype='float')))
    print("p value: ", get_p_value(float(worst_10_mhci[i][0]), np.asarray(results_mhci[:, 0], dtype='float')))
    print("-------------------------------")

print("\n")
print("------------ MHCII ------------")
print("-------------------------------")
print("-------- 10 Low Scored --------")
print("-------------------------------")
print("\n")

for i in range(len(best_10_mhcii)):
    random_list_mhcii = get_random_sequences(best_10_mhcii[i][1])
    results_mhcii = np.array(predict_scores('mhcii', random_list_mhcii))
    print("original sequence: ", best_10_mhcii[i])
    print("most immunogenic: ", results_mhcii[0])
    print("least immunogenic: ", results_mhcii[-1])
    print("score avg: ", np.mean(np.asarray(results_mhcii[:, 0], dtype='float')))
    print("score std: ", np.std(np.asarray(results_mhcii[:, 0], dtype='float')))
    print("p value: ", get_p_value(float(best_10_mhcii[i][0]), np.asarray(results_mhcii[:, 0], dtype='float')))
    print("-------------------------------")

print("\n")
print("-------------------------------")
print("------ 10 Middle Scored -------")
print("-------------------------------")
print("\n")

for i in range(len(middle_10_mhcii)):
    random_list_mhcii = get_random_sequences(middle_10_mhcii[i][1])
    results_mhcii = np.array(predict_scores('mhcii', random_list_mhcii))
    print("original sequence: ", middle_10_mhcii[i])
    print("most immunogenic: ", results_mhcii[0])
    print("least immunogenic: ", results_mhcii[-1])
    print("score avg: ", np.mean(np.asarray(results_mhcii[:, 0], dtype='float')))
    print("score std: ", np.std(np.asarray(results_mhcii[:, 0], dtype='float')))
    print("p value: ", get_p_value(float(middle_10_mhcii[i][0]), np.asarray(results_mhcii[:, 0], dtype='float')))
    print("-------------------------------")

print("\n")
print("-------------------------------")
print("----- 10 High Immunogenic -----")
print("-------------------------------")
print("\n")

for i in range(len(worst_10_mhcii)):
    random_list_mhcii = get_random_sequences(worst_10_mhcii[i][1])
    results_mhcii = np.array(predict_scores('mhcii', random_list_mhcii))
    print("original sequence: ", worst_10_mhcii[i])
    print("most immunogenic: ", results_mhcii[0])
    print("least immunogenic: ", results_mhcii[-1])
    print("score avg: ", np.mean(np.asarray(results_mhcii[:, 0], dtype='float')))
    print("score std: ", np.std(np.asarray(results_mhcii[:, 0], dtype='float')))
    print("p value: ", get_p_value(float(worst_10_mhcii[i][0]), np.asarray(results_mhcii[:, 0], dtype='float')))
    print("-------------------------------")
