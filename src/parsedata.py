import re

STANDARD_AAS = ['A','G','I','L','P','V','F','W','Y','D','E','R','H','K','S','T','C','M','N','Q']

def deindex_peptide_list( indexedlist ):

	peplist = [ deindex_peptide(peptide) for peptide in indexedlist ]

	return peplist


def deindex_peptide( peptide ):

	peptide = ''.join(x for x in peptide if x.isalpha())
	

	return peptide



def noindex_peptide( peptide ):

	peptide = list( peptide )

	peptide = ' '.join(peptide)

	return peptide


def noindex_peptide_list( peplist ):

	peplist = [ noindex_peptide(peptide) for peptide in peplist ]

	return peplist



def index_peptide( peptide ):

	peptide = list( peptide )

	peptide = [peptide[i] + str(i) for i in range( len(peptide) )]

	peptide = ' '.join(peptide)

	return peptide


def index_peptide_list( peplist ):

	peplist = [ index_peptide(peptide) for peptide in peplist ]

	return peplist


def deindex( target ):
	
	if type( target ) == str:
		return deindex_peptide( target )

	if type(target) == list:
		return deindex_peptide_list( target )


def noindex( target ):

	if type( target ) == str:
		return noindex_peptide( target )

	if type(target) == list:
		return noindex_peptide_list( target )


def index( target ):
	
	if type( target ) == str:
		return index_peptide( target )

	if type(target) == list:
		return index_peptide_list( target )
	


def filter_peptidelist_w_non_standard( peptidelist ):

	# finds peptides with nonSTANDARD_AAS AAs
	has_non_standard = [peptide for peptide in peptidelist for aa in peptide if aa not in STANDARD_AAS]
	
	# removes peptides containing non-std AAs
	only_w_standard = [peptide for peptide in peptidelist if peptide not in has_non_standard]

	return only_w_standard



def filter_non_standard( target ):

	# in case 'target' is a single peptide
	if type(target) == str:
		only_w_standard = ''.join(filter_peptidelist_w_non_standard( [target] ))
	
	
	# in case 'target' is a list of peptides
	if type(target) == list:
		only_w_standard = filter_peptidelist_w_non_standard( target )


	return only_w_standard




def clean_str(string):

	string = re.sub(r"[^A-Za-z0-9]\-", " ", string)
	string = re.sub(r"\'s", " \'s", string)
	string = re.sub(r"\'ve", " \'ve", string)
	string = re.sub(r"n\'t", " n\'t", string)
	string = re.sub(r"\'re", " \'re", string)
	string = re.sub(r"\'d", " \'d", string)
	string = re.sub(r"\'ll", " \'ll", string)
	string = re.sub(r",", " , ", string)
	string = re.sub(r"!", " ! ", string)
	string = re.sub(r"\(", " \( ", string)
	string = re.sub(r"\)", " \) ", string)
	string = re.sub(r"\?", " \? ", string)
	string = re.sub(r"\s{2,}", " ", string)
	return string.strip().upper()


def load_data_and_labels( filelist, indexing ):
	
	examples = [] # x data; comes from filelist
	labels   = [] # y labels; generated here
	
	
	for i,datafile in enumerate(filelist):

		# parse file
		lines = open(datafile, "r", encoding='utf-8').readlines()
		lines = [s.strip() for s in lines]
		
		# get sequences
		examples += list(lines)
		
		# create labels
		labels += len(lines) * [i]


	if indexing:
		examples = index( examples )
	else:
		examples = noindex( examples )


	return examples, labels




def create_dict( indexing=False ):

	if indexing:
		AAs = [ res+str(i) for i in range(-30,30) for res in STANDARD_AAS]
	else:
		AAs = [ res for i in range(-30,30) for res in STANDARD_AAS]


	dictionary = {}
	for i,res in zip(range(0,len( AAs )), AAs):
		dictionary[ res ] = i
	
	return dictionary



def translate( peptide_list, indexing=False ):

	dictionary = create_dict( indexing )
	
	translated_peptides = [ ]

	for peptide in peptide_list:
		translated_pep = []

		for aminoacid in peptide.split(' '):
			if aminoacid in dictionary:
				translated_pep.append( dictionary[aminoacid] )

		translated_peptides.append( translated_pep )
	
	return translated_peptides







## -- old 2-class classification function
#def load_data_and_labels(negative_data_file, positive_data_file):

	#"""
	#Loads MR polarity data from files, splits the data fo words and generates labels.
	#Returns split sentences and labels.
	#"""

	#positive_examples = list(open(positive_data_file, "r", encoding='utf-8').readlines())
	#positive_examples = [s.strip() for s in positive_examples]

	#negative_examples = list(open(negative_data_file, "r", encoding='utf-8').readlines())
	#negative_examples = [s.strip() for s in negative_examples]


	## Generate labels
	#positive_labels = [ 1 for _ in positive_examples]
	#negative_labels = [ 0 for _ in negative_examples]


	## joins positive and negative examples into x
	#x_text = positive_examples + negative_examples
	#x_text = [clean_str(sent) for sent in x_text]

	## joins positive and negative labels into y
	#y = positive_labels + negative_labels

	#return [x_text, y]
