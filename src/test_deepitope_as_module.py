#!/usr/bin/python3

import deepitope as dp

pred_mhci = dp.predict('mhci', 'SpikeWT2.fasta')
pred_mhcii = dp.predict('mhcii', 'SpikeWT2.fasta')

print('Most immunogenic mhci epitope: ' + str(pred_mhci[0][1]))
print('Most immunogenic mhcii epitope: ' + str(pred_mhcii[0][1]))

print(pred_mhci[0])
print(pred_mhcii[0])
