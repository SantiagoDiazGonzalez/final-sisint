import deepitope as dp
import parsedata
import random
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import sys
import itertools
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import sequence
matplotlib.use('Agg')

STANDARD_AAS = ['A', 'G', 'I', 'L', 'P', 'V', 'F', 'W', 'Y', 'D', 'E', 'R', 'H', 'K', 'S', 'T', 'C', 'M', 'N', 'Q']


def get_bin_index(val, _bins):
    bin_index = 0
    for _bin in _bins:
        if val < _bin:
            return bin_index - 1
        bin_index = bin_index + 1
    return bin_index - 1


def get_p_value(_score, _all_scores):
    p = 0
    for s in _all_scores:
        if _score >= s:
            p = p + 1
    return p / len(_all_scores)


def search_by_score(_score, _results):
    for _result in _results:
        if _score <= float(_result[0]):
            return _result
    return _results[len(_results)-1]


def shuffle_word(word, seed):
    word = list(word)
    random.Random(seed).shuffle(word)
    return ''.join(word)


def get_random_sequences(pred):
    _random_list = []
    for i in range(15000):
        _random_list.append(shuffle_word(pred, i))
    _random_list = list(set(_random_list))    # getting unique values
    if pred in _random_list:
        _random_list.remove(pred)
    _random_list = _random_list[:1000]    # getting only 1000 values
    if len(_random_list) != 1000:
        _random_list = []
        perm = list(itertools.permutations(pred))
        perm = perm[1:1001]  # getting only 1000 values
        for word in perm:
            _random_list.append(''.join(word))
    return sorted(_random_list)


def seq_to_string(sequence_list):
    sequences_string = ''
    for sequence in sequence_list:
        sequences_string = sequences_string + sequence
    return sequences_string


def predict_scores(model, randomlist):
    ''' Searches 'model' dir for trained models of all
        sizes and returns a tuple with the resulting
        preds: (score, peptide, start_pos, end_pos) '''

    sizes = [sizedir for sizedir in os.listdir(model)
             if os.path.isdir(os.path.join(model, sizedir))]

    for size in sizes:

        modeldir = os.path.join(model, size)
        model = load_model(modeldir)

        indexed_list = parsedata.index_peptide_list(randomlist)

        x_test = parsedata.translate(indexed_list, True)
        x_test = sequence.pad_sequences(x_test)

        # convert to numpy
        x_test = np.asarray(x_test)
        x_test = x_test.astype(float)

        # uses trained model to predict
        y_score = model.predict(x_test)

        # forms a list of tuples with all predictions:
        results = []
        for seq, score, pos in zip(randomlist, y_score, range(0, len(randomlist))):
            results += [(round(score[0], 3), seq, pos, pos + int(size) - 1)]

    return sorted(results)


def split(word):
    return [char for char in word]


def get_mutated_sequences(sequence):
    new_sequences = []
    temp = sequence
    for j in range(len(temp)):
        temp_sequence = split(temp)
        for k in STANDARD_AAS:
            if temp[j] != k:
                temp_sequence[j] = k
                new_sequences.append(''.join(temp_sequence))
    return new_sequences


print("Score Input (values from 0 t0 1)")
score_input = input()

if not os.path.exists('tests/candidate/'+score_input+'/'):
    os.makedirs('tests/candidate/'+score_input+'/')

# Write everything to file
sys.stdout = open("tests/candidate/"+score_input+"/results.txt", "w+")

# Getting predictions from fasta file
pred_mhci = np.array(dp.predict('mhci', 'SpikeWT2.fasta'))
pred_mhcii = np.array(dp.predict('mhcii', 'SpikeWT2.fasta'))

# Getting peptides to test by score
test_mhci = search_by_score(float(score_input), pred_mhci)
test_mhcii = search_by_score(float(score_input), pred_mhcii)

# Plotting histogram for MHCI scores
n, bins, patches = plt.hist(np.asarray(pred_mhci[:, 0], dtype='float'), bins=np.arange(0, 1, 0.01))
i = get_bin_index(float(test_mhci[0]), bins)
patches[i].set_fc('r')
plt.title("MHCI Histogram")
plt.xticks(np.arange(0, 1.1, 0.1))
plt.savefig('tests/candidate/'+score_input+'/mhci_hist.png', bbox_inches='tight')
plt.clf()

# Plotting histogram for MHCII scores
n, bins, patches = plt.hist(np.asarray(pred_mhcii[:, 0], dtype='float'), bins=np.arange(0, 1, 0.01))
i = get_bin_index(float(test_mhcii[0]), bins)
patches[i].set_fc('r')
plt.title("MHCII Histogram")
plt.xticks(np.arange(0, 1.1, 0.1))
plt.savefig('tests/candidate/'+score_input+'/mhcii_hist.png', bbox_inches='tight')
plt.clf()

# Getting p values for MHCI and MHCII scores
all_scores_mhci = np.asarray(pred_mhci[:, 0], dtype='float')
all_scores_mhcii = np.asarray(pred_mhcii[:, 0], dtype='float')

scores = [test_mhci[0], test_mhcii[0]]
sequences = [test_mhci[1], test_mhcii[1]]

scores = np.array(scores).flatten()
sequences = np.array(sequences).flatten()

tabla = np.vstack((scores, sequences)).T

df = pd.DataFrame(data=tabla, columns=["score", "sequence"])

print("-------------------------------")
print("---- Secuencias a testear -----")
print("-------------------------------")
print(df)
print("\n")


# Null distribution testing
random_list_mhci = get_random_sequences(test_mhci[1])
random_list_mhcii = get_random_sequences(test_mhcii[1])

results_mhci = np.array(predict_scores('mhci', random_list_mhci))
results_mhcii = np.array(predict_scores('mhcii', random_list_mhcii))

print("-------------------------------")
print("-- Null Distribution Testing --")
print("-------------------------------")

print("original sequence: ", test_mhci)
print("most immunogenic: ", results_mhci[0])
print("least immunogenic: ", results_mhci[-1])
print("score avg: ", np.mean(np.asarray(results_mhci[:, 0], dtype='float')))
print("score std: ", np.std(np.asarray(results_mhci[:, 0], dtype='float')))
print("p value: ", get_p_value(float(test_mhci[0]), np.asarray(results_mhci[:, 0], dtype='float')))
pd.DataFrame(results_mhci).to_csv('tests/candidate/'+score_input+'/null_distrib_mhci.csv')

print("-------------------------------")

print("original sequence: ", test_mhcii)
print("most immunogenic: ", results_mhcii[0])
print("least immunogenic: ", results_mhcii[-1])
print("score avg: ", np.mean(np.asarray(results_mhcii[:, 0], dtype='float')))
print("score std: ", np.std(np.asarray(results_mhcii[:, 0], dtype='float')))
print("p value: ", get_p_value(float(test_mhcii[0]), np.asarray(results_mhcii[:, 0], dtype='float')))
pd.DataFrame(results_mhcii).to_csv('tests/candidate/'+score_input+'/null_distrib_mhcii.csv')

# Null distribtion MHCI histogram
n, bins, patches = plt.hist(np.asarray(results_mhci[:, 0], dtype='float'), bins=np.arange(0, 1, 0.01))
i = get_bin_index(float(test_mhci[0]), bins)
patches[i].set_fc('r')
plt.title("MHCI Null Distribution Histogram")
plt.xticks(np.arange(0, 1.1, 0.1))
plt.savefig('tests/candidate/'+score_input+'/null_mhci_hist.png', bbox_inches='tight')
plt.clf()

# Null distribtion MHCII histogram
n, bins, patches = plt.hist(np.asarray(results_mhcii[:, 0], dtype='float'), bins=np.arange(0, 1, 0.01))
i = get_bin_index(float(test_mhcii[0]), bins)
patches[i].set_fc('r')
plt.title("MHCII Null Distribution Histogram")
plt.xticks(np.arange(0, 1.1, 0.1))
plt.savefig('tests/candidate/'+score_input+'/null_mhcii_hist.png', bbox_inches='tight')
plt.clf()

# Mutation test
mutated_sequences_mhci = get_mutated_sequences(test_mhci[1])
mutated_sequences_mhcii = get_mutated_sequences(test_mhcii[1])

results_mhci = np.array(predict_scores('mhci', mutated_sequences_mhci))
results_mhcii = np.array(predict_scores('mhcii', mutated_sequences_mhcii))

print("\n")
print("-------------------------------")
print("------ Mutation Testing -------")
print("-------------------------------")

print("original sequence: ", test_mhci)
print("most immunogenic: ", results_mhci[0])
print("least immunogenic", results_mhci[-1])
print("score avg: ", np.mean(np.asarray(results_mhci[:, 0], dtype='float')))
print("score std: ", np.std(np.asarray(results_mhci[:, 0], dtype='float')))
print("p value: ", get_p_value(float(test_mhci[0]), np.asarray(results_mhci[:, 0], dtype='float')))
pd.DataFrame(results_mhci).to_csv('tests/candidate/'+score_input+'/mutation_mhci.csv')

print("-------------------------------")

print("original sequence: ", test_mhcii)
print("most immunogenic: ", results_mhcii[0])
print("least immunogenic: ", results_mhcii[-1])
print("score avg: ", np.mean(np.asarray(results_mhcii[:, 0], dtype='float')))
print("score std: ", np.std(np.asarray(results_mhcii[:, 0], dtype='float')))
print("p value: ", get_p_value(float(test_mhcii[0]), np.asarray(results_mhcii[:, 0], dtype='float')))
pd.DataFrame(results_mhcii).to_csv('tests/candidate/'+score_input+'/mutation_mhcii.csv')
