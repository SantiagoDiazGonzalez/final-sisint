import deepitope as dp
import parsedata
import random
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import sys
import itertools
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import sequence
matplotlib.use('Agg')

STANDARD_AAS = ['A', 'G', 'I', 'L', 'P', 'V', 'F', 'W', 'Y', 'D', 'E', 'R', 'H', 'K', 'S', 'T', 'C', 'M', 'N', 'Q']


def get_bin_index(val, _bins):
    bin_index = 0
    for _bin in _bins:
        if val < _bin:
            return bin_index - 1
        bin_index = bin_index + 1
    return bin_index - 1


def get_p_value(_score, _all_scores):
    p = 0
    for s in _all_scores:
        if _score >= s:
            p = p + 1
    return p / len(_all_scores)


def shuffle_word(word, seed):
    word = list(word)
    random.Random(seed).shuffle(word)
    return ''.join(word)


def get_random_sequences(pred):
    _random_list = []
    for i in range(15000):
        _random_list.append(shuffle_word(pred, i))
    _random_list = list(set(_random_list))    # getting unique values
    if pred in _random_list:
        _random_list.remove(pred)
    _random_list = _random_list[:1000]    # getting only 1000 values
    if len(_random_list) != 1000:
        _random_list = []
        perm = list(itertools.permutations(pred))
        perm = perm[1:1001]  # getting only 1000 values
        for word in perm:
            _random_list.append(''.join(word))
    return sorted(_random_list)


def seq_to_string(sequence_list):
    sequences_string = ''
    for sequence in sequence_list:
        sequences_string = sequences_string + sequence
    return sequences_string


def predict_scores(model, randomlist):
    ''' Searches 'model' dir for trained models of all
        sizes and returns a tuple with the resulting
        preds: (score, peptide, start_pos, end_pos) '''

    sizes = [sizedir for sizedir in os.listdir(model)
             if os.path.isdir(os.path.join(model, sizedir))]

    for size in sizes:

        modeldir = os.path.join(model, size)
        model = load_model(modeldir)

        indexed_list = parsedata.index_peptide_list(randomlist)

        x_test = parsedata.translate(indexed_list, True)
        x_test = sequence.pad_sequences(x_test)

        # convert to numpy
        x_test = np.asarray(x_test)
        x_test = x_test.astype(float)

        # uses trained model to predict
        y_score = model.predict(x_test)

        # forms a list of tuples with all predictions:
        results = []
        for seq, score, pos in zip(randomlist, y_score, range(0, len(randomlist))):
            results += [(round(score[0], 3), seq, pos, pos + int(size) - 1)]

    return sorted(results)


def split(word):
    return [char for char in word]


def get_mutated_sequences(sequence):
    new_sequences = []
    temp = sequence
    for j in range(len(temp)):
        temp_sequence = split(temp)
        for k in STANDARD_AAS:
            if temp[j] != k:
                temp_sequence[j] = k
                new_sequences.append(''.join(temp_sequence))
    return new_sequences


print("peptide Input")
seq_input = input()

# Getting predictions from fasta file
if len(seq_input) == 9:
    pred = np.array(dp.predict('mhci', 'SpikeWT2.fasta'))
elif len(seq_input) == 15:
    pred = np.array(dp.predict('mhcii', 'SpikeWT2.fasta'))
else:
    sys.exit("Sequence should be mhci or mhcii")

if not os.path.exists('tests/sequence/' + seq_input + '/'):
    os.makedirs('tests/sequence/' + seq_input + '/')

# Write everything to file
sys.stdout = open("tests/sequence/" + seq_input + "/results.txt", "w+")

if seq_input not in pred[:, 1]:
    seq_list = [seq_input]
    pred = np.array(predict_scores('mhci', seq_list))
    test = pred[0]
else:
    for prediction in pred:
        if prediction[1] == seq_input:
            test = prediction

    # Plotting histogram
    n, bins, patches = plt.hist(np.asarray(pred[:, 0], dtype='float'), bins=np.arange(0, 1, 0.01))
    i = get_bin_index(float(test[0]), bins)
    patches[i].set_fc('r')
    plt.title("Scores Histogram")
    plt.xticks(np.arange(0, 1.1, 0.1))
    plt.savefig('tests/sequence/' + seq_input + '/hist.png', bbox_inches='tight')
    plt.clf()

print("-------------------------------")
print("------- Testing Sequence ------")
print("-------------------------------")
print("Score: ", test[0], "    Sequence: ", test[1])
print("\n")

# Null distribution testing
random_list = get_random_sequences(test[1])
if len(seq_input) == 9:
    results = np.array(predict_scores('mhci', random_list))
elif len(seq_input) == 15:
    results = np.array(predict_scores('mhcii', random_list))

p_values = []
for result in results:
    p_values.append(get_p_value(float(result[0]), np.asarray(results[:, 0], dtype='float')))

print("-------------------------------")
print("-- Null Distribution Testing --")
print("-------------------------------")

print("original sequence: ", test)
print("most immunogenic: ", results[0])
print("least immunogenic: ", results[-1])
print("score avg: ", np.mean(np.asarray(results[:, 0], dtype='float')))
print("score std: ", np.std(np.asarray(results[:, 0], dtype='float')))
p_val = get_p_value(float(test[0]), np.asarray(results[:, 0], dtype='float'))
print("p value: ", p_val)

# Plotting
plt.boxplot(p_values)
plt.title(seq_input + " - Null Distribution p-values Boxplot")
plt.savefig('tests/sequence/' + seq_input + '/p_val_null_boxplot.png', bbox_inches='tight')
plt.clf()

scores = np.asarray(results[:, 0], dtype='float')
sequences = np.asarray(results[:, 1])
p_values = np.asarray(p_values, dtype="float")

tabla = np.vstack((scores, sequences, p_values)).T
pd.DataFrame(tabla).to_csv('tests/sequence/' + seq_input + '/null_distrib.csv')


# Null distribtion MHCI histogram
n, bins, patches = plt.hist(np.asarray(results[:, 0], dtype='float'), bins=np.arange(0, np.max(scores), 0.01))
i = get_bin_index(float(test[0]), bins)
patches[i].set_fc('r')
plt.title(seq_input + " - Null Distribution Scores Histogram")
plt.xticks(np.arange(0, np.max(scores)+0.001, 0.1))
plt.savefig('tests/sequence/' + seq_input + '/null_hist.png', bbox_inches='tight')
plt.clf()


# Mutation test
mutated_sequences = get_mutated_sequences(test[1])
if len(seq_input) == 9:
    results = np.array(predict_scores('mhci', mutated_sequences))
elif len(seq_input) == 15:
    results = np.array(predict_scores('mhcii', mutated_sequences))

p_values = []
for result in results:
    p_values.append(get_p_value(float(result[0]), np.asarray(results[:, 0], dtype='float')))

print("\n")
print("-------------------------------")
print("------ Mutation Testing -------")
print("-------------------------------")

print("original sequence: ", test)
print("most immunogenic: ", results[0])
print("least immunogenic", results[-1])
print("score avg: ", np.mean(np.asarray(results[:, 0], dtype='float')))
print("score std: ", np.std(np.asarray(results[:, 0], dtype='float')))
p_val = get_p_value(float(test[0]), np.asarray(results[:, 0], dtype='float'))
print("p value: ", p_val)

# Plotting
plt.boxplot(p_values)
plt.title(seq_input + " - Mutation p-values Boxplot")
plt.savefig('tests/sequence/' + seq_input + '/p_val_mutation_boxplot.png', bbox_inches='tight')
plt.clf()

scores = np.asarray(results[:, 0], dtype='float')
sequences = np.asarray(results[:, 1])
p_values = np.asarray(p_values, dtype="float")

tabla = np.vstack((scores, sequences, p_values)).T
pd.DataFrame(tabla).to_csv('tests/sequence/' + seq_input + '/mutation.csv')

# Mutation histogram
n, bins, patches = plt.hist(np.asarray(results[:, 0], dtype='float'), bins=np.arange(0, np.max(scores), 0.01))
i = get_bin_index(float(test[0]), bins)
patches[i].set_fc('r')
plt.title(seq_input + " - Mutation Scores Histogram")
plt.xticks(np.arange(0, np.max(scores)+0.001, 0.1))
plt.savefig('tests/sequence/' + seq_input + '/mutation_hist.png', bbox_inches='tight')
plt.clf()
