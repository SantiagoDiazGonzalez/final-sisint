import deepitope as dp
import parsedata
import random
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import itertools
import sys
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import sequence
matplotlib.use('Agg')

STANDARD_AAS = ['A', 'G', 'I', 'L', 'P', 'V', 'F', 'W', 'Y', 'D', 'E', 'R', 'H', 'K', 'S', 'T', 'C', 'M', 'N', 'Q']


def get_bin_index(val, _bins):
    bin_index = 0
    for _bin in _bins:
        if val < _bin:
            return bin_index - 1
        bin_index = bin_index + 1
    return bin_index - 1


def get_p_value(_score, _all_scores):
    p = 0
    for s in _all_scores:
        if _score >= s:
            p = p + 1
    return p / len(_all_scores)


def shuffle_word(word, seed):
    word = list(word)
    random.Random(seed).shuffle(word)
    return ''.join(word)


def get_random_sequences(pred):
    _random_list = []
    for i in range(15000):
        _random_list.append(shuffle_word(pred, i))
    _random_list = list(set(_random_list))    # getting unique values
    if pred in _random_list:
        _random_list.remove(pred)
    _random_list = _random_list[:1000]    # getting only 1000 values
    if len(_random_list) != 1000:
        _random_list = []
        perm = list(itertools.permutations(pred))
        perm = perm[1:1001]  # getting only 1000 values
        for word in perm:
            _random_list.append(''.join(word))
    return sorted(_random_list)


def seq_to_string(sequence_list):
    sequences_string = ''
    for sequence in sequence_list:
        sequences_string = sequences_string + sequence
    return sequences_string


def predict_scores(model, randomlist):
    ''' Searches 'model' dir for trained models of all
        sizes and returns a tuple with the resulting
        preds: (score, peptide, start_pos, end_pos) '''

    sizes = [sizedir for sizedir in os.listdir(model)
             if os.path.isdir(os.path.join(model, sizedir))]

    for size in sizes:

        modeldir = os.path.join(model, size)
        model = load_model(modeldir)

        indexed_list = parsedata.index_peptide_list(randomlist)

        x_test = parsedata.translate(indexed_list, True)
        x_test = sequence.pad_sequences(x_test)

        # convert to numpy
        x_test = np.asarray(x_test)
        x_test = x_test.astype(float)

        # uses trained model to predict
        y_score = model.predict(x_test)

        # forms a list of tuples with all predictions:
        results = []
        for seq, score, pos in zip(randomlist, y_score, range(0, len(randomlist))):
            results += [(round(score[0], 3), seq, pos, pos + int(size) - 1)]

    return sorted(results)


print('Getting Predictions...')
# Getting predictions from fasta file
pred_mhci = np.array(dp.predict('mhci', 'SpikeWT2.fasta'))
pred_mhcii = np.array(dp.predict('mhcii', 'SpikeWT2.fasta'))

if not os.path.exists('tests/tops/p-values/'):
    os.makedirs('tests/tops/p-values/')

print('Getting MHCI and MHCII p-values...')
# Write everything to file
sys.stdout = open("tests/tops/p-values/results.txt", "w+")

# Getting best, worst and middle scored MHCI sequences
best_10_mhci = pred_mhci[:10]
index = int(round(len(pred_mhci) / 2)) - 5
middle_10_mhci = pred_mhci[index:index + 10]
worst_10_mhci = pred_mhci[-10:]

# Getting best, worst and middle scored MHCII sequences
best_10_mhcii = pred_mhcii[:10]
index = int(round(len(pred_mhcii) / 2)) - 5
middle_10_mhcii = pred_mhcii[index:index + 10]
worst_10_mhcii = pred_mhcii[-10:]

p_values_top10_mhci = []
p_values_middle10_mhci = []
p_values_worst10_mhci = []

saved_results = []
for seq in best_10_mhci:
    random_list = get_random_sequences(seq[1])
    results = np.array(predict_scores('mhci', random_list))
    saved_results.append(results)
    for result in results:
        p_values_top10_mhci.append(get_p_value(float(result[0]), np.asarray(results[:, 0], dtype='float')))

saved_results = np.array(saved_results)
saved_results = np.concatenate(saved_results, axis=0)
scores = np.asarray(saved_results[:, 0], dtype='float')
sequences = np.asarray(saved_results[:, 1])
p_values = np.asarray(p_values_top10_mhci, dtype="float")
tabla = np.vstack((scores, sequences, p_values)).T
pd.DataFrame(tabla).to_csv('tests/tops/p-values/best_10_mhci.csv')

saved_results = []
for seq in middle_10_mhci:
    random_list = get_random_sequences(seq[1])
    results = np.array(predict_scores('mhci', random_list))
    saved_results.append(results)
    for result in results:
        p_values_middle10_mhci.append(get_p_value(float(result[0]), np.asarray(results[:, 0], dtype='float')))

saved_results = np.array(saved_results)
saved_results = np.concatenate(saved_results, axis=0)
scores = np.asarray(saved_results[:, 0], dtype='float')
sequences = np.asarray(saved_results[:, 1])
p_values = np.asarray(p_values_middle10_mhci, dtype="float")
tabla = np.vstack((scores, sequences, p_values)).T
pd.DataFrame(tabla).to_csv('tests/tops/p-values/middle_10_mhci.csv')

saved_results = []
for seq in worst_10_mhci:
    random_list = get_random_sequences(seq[1])
    results = np.array(predict_scores('mhci', random_list))
    saved_results.append(results)
    for result in results:
        p_values_worst10_mhci.append(get_p_value(float(result[0]), np.asarray(results[:, 0], dtype='float')))

saved_results = np.array(saved_results)
saved_results = np.concatenate(saved_results, axis=0)
scores = np.asarray(saved_results[:, 0], dtype='float')
sequences = np.asarray(saved_results[:, 1])
p_values = np.asarray(p_values_worst10_mhci, dtype="float")
tabla = np.vstack((scores, sequences, p_values)).T
pd.DataFrame(tabla).to_csv('tests/tops/p-values/worst_10_mhci.csv')


p_values_top10_mhcii = []
p_values_middle10_mhcii = []
p_values_worst10_mhcii = []

saved_results = []
for seq in best_10_mhcii:
    random_list = get_random_sequences(seq[1])
    results = np.array(predict_scores('mhcii', random_list))
    saved_results.append(results)
    for result in results:
        p_values_top10_mhcii.append(get_p_value(float(result[0]), np.asarray(results[:, 0], dtype='float')))

saved_results = np.array(saved_results)
saved_results = np.concatenate(saved_results, axis=0)
scores = np.asarray(saved_results[:, 0], dtype='float')
sequences = np.asarray(saved_results[:, 1])
p_values = np.asarray(p_values_top10_mhcii, dtype="float")
tabla = np.vstack((scores, sequences, p_values)).T
pd.DataFrame(tabla).to_csv('tests/tops/p-values/best_10_mhcii.csv')

saved_results = []
for seq in middle_10_mhcii:
    random_list = get_random_sequences(seq[1])
    results = np.array(predict_scores('mhcii', random_list))
    saved_results.append(results)
    for result in results:
        p_values_middle10_mhcii.append(get_p_value(float(result[0]), np.asarray(results[:, 0], dtype='float')))

saved_results = np.array(saved_results)
saved_results = np.concatenate(saved_results, axis=0)
scores = np.asarray(saved_results[:, 0], dtype='float')
sequences = np.asarray(saved_results[:, 1])
p_values = np.asarray(p_values_middle10_mhcii, dtype="float")
tabla = np.vstack((scores, sequences, p_values)).T
pd.DataFrame(tabla).to_csv('tests/tops/p-values/middle_10_mhcii.csv')

saved_results = []
for seq in worst_10_mhcii:
    random_list = get_random_sequences(seq[1])
    results = np.array(predict_scores('mhcii', random_list))
    saved_results.append(results)
    for result in results:
        p_values_worst10_mhcii.append(get_p_value(float(result[0]), np.asarray(results[:, 0], dtype='float')))

saved_results = np.array(saved_results)
saved_results = np.concatenate(saved_results, axis=0)
scores = np.asarray(saved_results[:, 0], dtype='float')
sequences = np.asarray(saved_results[:, 1])
p_values = np.asarray(p_values_worst10_mhcii, dtype="float")
tabla = np.vstack((scores, sequences, p_values)).T
pd.DataFrame(tabla).to_csv('tests/tops/p-values/worst_10_mhcii.csv')
saved_results = []

data_to_plot = [p_values_top10_mhci, p_values_middle10_mhci, p_values_worst10_mhci]

fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot, patch_artist=True)

for box in bp['boxes']:
    box.set(color='#7570b3', linewidth=2)
    box.set(facecolor='#1b9e77')

for whisker in bp['whiskers']:
    whisker.set(color='#7570b3', linewidth=2)

for cap in bp['caps']:
    cap.set(color='#7570b3', linewidth=2)

for median in bp['medians']:
    median.set(color='#b2df8a', linewidth=2)

for flier in bp['fliers']:
    flier.set(marker='o', color='#e7298a', alpha=0.5)

ax.set_xticklabels(['First 10', 'Middle 10', 'Last 10'])
plt.title("MHCI Null Distribution p-values Boxplots")

fig.savefig('tests/tops/p-values/mhci.png', bbox_inches='tight')
plt.clf()


data_to_plot2 = [p_values_top10_mhcii, p_values_middle10_mhcii, p_values_worst10_mhcii]

fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot2, patch_artist=True)

for box in bp['boxes']:
    box.set(color='#7570b3', linewidth=2)
    box.set(facecolor='#1b9e77')
for whisker in bp['whiskers']:
    whisker.set(color='#7570b3', linewidth=2)
for cap in bp['caps']:
    cap.set(color='#7570b3', linewidth=2)
for median in bp['medians']:
    median.set(color='#b2df8a', linewidth=2)
for flier in bp['fliers']:
    flier.set(marker='o', color='#e7298a', alpha=0.5)

ax.set_xticklabels(['First 10', 'Middle 10', 'Last 10'])
plt.title("MHCII Null Distribution p-values Boxplots")

fig.savefig('tests/tops/p-values/mhcii.png', bbox_inches='tight')