#!/usr/bin/python3
import sys

from absl import logging
logging._warn_preinit_stderr = 0

import logging
logging.getLogger('tensorflow').disabled = True

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import sequence

import argparse 
import numpy

import parsedata



# -- Initializes parser ---
parser = argparse.ArgumentParser(description='Deepitope: epitope prediction program.')

# MANDATORY arguments
parser.add_argument( '-i', required=True, type= str, metavar='file', help='Fasta file' )
parser.add_argument( '-m', required=True, type=str,   metavar='dir', help='Model dir' )

# optional arguments
parser.add_argument( '-o', required=False, type=str,   metavar='file', help='Outputname' )

## don't use it yet
#parser.add_argument('--indexing', dest='indexing', action='store_false',   help='Indexes residues', default='True')


def get_sequences( fastalines, size ):
	
	''' returns only the sequences 
	    of a fasta file '''

	seq  = ''
	seqs = []
	fastalines += ['>'] 
	
	
	for line in fastalines:
		if line[0] == '>' :
			if len(seq) > size:

				seqs.append( seq )

			seq  = ''
			continue

		else:
			seq += line.rstrip()

	return seqs




def get_peptides_from_seqs( sequences, size ):
	
	''' splits sequence into chunks 
	    of the specified size '''
	
	peptides = []
	
	for seq in sequences:
		for i in range(len(seq)-size+1):
			peptide = seq[i:i+size]

			# checks for non-standard aminoacids
			peptides += [ parsedata.filter_non_standard(peptide) ]

	
	return peptides




def get_peptides_from_fasta( inputfastafile, size ):

	''' Returns peptides of the specified
	    size from a fasta file. Wrapper 
	    for the functions 'get_sequences'
	    and 'get_peptides_from_seqs' '''
	 

	with open( inputfastafile ) as fastafile:
		fastas = fastafile.readlines()

	sequences = get_sequences( fastas, int(size) )
	sequences = get_peptides_from_seqs( sequences, int(size) )

	return sequences



def predict( model, fasta ):
	
	''' Searches 'model' dir for trained models of all
		sizes and returns a tuple with the resulting
		preds: (score, peptide, start_pos, end_pos) '''	

	sizes = [sizedir for sizedir in os.listdir(model) 
		  if os.path.isdir(os.path.join(model,sizedir))]

	for size in sizes:

		modeldir = os.path.join(model, size)
	
		peptides = get_peptides_from_fasta( fasta, size )

		model = load_model( modeldir )

		# indexing residues is still used. Maybe we'll remove it in the future
		indexed_peptides = parsedata.index_peptide_list( peptides )

		x_test = parsedata.translate( indexed_peptides, True )
		x_test = sequence.pad_sequences(x_test)

		# convert to numpy
		x_test = numpy.asarray( x_test )
		x_test = x_test.astype( float )

		# uses trained model to predict 
		y_score = model.predict( x_test )
		
		# forms a list of tuples with all predictions:
		results = []
		for seq, score, pos in zip(peptides, y_score, range(1,len(peptides))):
			results += [(round(score[0],3), seq, pos, pos+int(size)-1)]


	return sorted(results)


def format_as_string( prediction ):
	
	''' takes a prediction as a list 
	    of tuples and converts to a 
	    string for some nice printing '''
	
	strpred = ''
	for p in prediction:
		strpred += ''.join(str(p).replace(')','').replace('(','').replace(',','')) + '\n'
	
	return strpred



if __name__ == "__main__":
		
	arg = parser.parse_args()

	pred = predict( arg.m, arg.i )
	
	to_print = format_as_string( pred )
	
	if arg.o: # print to file
		with open( arg.o, 'w' ) as outputfile:
			outputfile.write( to_print )

	else: # print to screen
		print( to_print[:-1] )

