El problema
 
Hello Elmer,
I trained two models for you: one with MHC-I for 9-mers and one with MHC-II with 15-mers. The results are included in the files mhci.txt and mhcii.txt, respectively. The output is sorted by immunogenicity from 0 to 1, and the closest to 0, the higher the chances the peptide is an epitope. Values closer to 1 are the least immunogenic. From the output, I can tell you that there are many immunogenic peptides in this protein as values are very close to 0.
 
Output format:
[ score ]  [ peptide ]  [ starting position ]   [ ending position ]
0.073     ASDFHAUOIHAF  43   58
 
I've also included a standalone version of deepitope I used to generate the predictions, in case you wanna run your own predictions for other files. 
 
Usage for MHC-I :
./src/deepitope.py mhci SpikeWT2.fasta
 
for MHC-II
./src/deepitope.py mhcii SpikeWT2.fasta
 
Each prediction overwrites the output file mhci.txt or mhcii.txt. You'll need python-tensorflow v2.0, python-keras and python-numpy installed. 
 
Would you like to compare to what other methods also predict? It would be great if you gave us any insight as to how to improve deepitope.
Best,
 
---------------------

Chicos/as/es
El párrafo anterior es de unos colegas brasileños que están desarrollando un modelo neuronal para predecir si un segmento de una proteína (péptido o epitope) es capaz de generar una respuesta inmune.

En los archivos “mhci.txt” y “mhcii.txt” disponinbles en su carpeta son las respectivas salidas de esas 2 redes sobre la proteina Spike del virus SARS-Cov2 o Covid-19. 
El archivo SpikeWT.fasta, (archivo fasta) disponible en su directorio, es un archivo de texto como el siguiente (un pedazo del archivo, Uds lo pueden abrir cpn cualquier editor de texto):
>YP_009724390.1 surface glycoprotein [Severe acute respiratory syndrome coronavirus 2]
MFVFLVLLPLVSSQCVNLTTRTQLPPAYTNSFTRGVYYPDKVFRSSVLHSTQDLFLPFFSNVTWFHAIHVSGTNGTKRFDNPVLPFNDGVYFASTEKSNIIRGWIFGTTLDSKTQSLLIVNNATNVVIKVCEFQFCNDPF

El algoritomo deepitope.py mhci recorre esta secuencia por segmentor de 9mers (9 letras) y el deepitope.py mhcii sobre segmentos de 15mers (15 letras), es decir, que en el mhci evalua
pripero     MFVFLVLLP
segundo     FVFLVLLPL
y así sucesivamente (ambos softwares hacen lo mismo)
para cada segmento, el modelo proporciona un score (salida de la ultima neurona) entre 
0 -> “Candidato inmunogenico”, 1 -> “No es un candidato”
por ejemplo 
[ score ]  [ peptide ]              [ starting position ]   [ ending position ]
0.073     ASDFHAUOI   43                             58
lo que quiere decir que la secuencia (peptido-epitope) ASDFHAUOI, tiene chances de ser un candidato inmunogenico.
El problema es que no sabemos cuán confiable es ese valor. Para lo cual proponemos evaluar lo siguiente:
 
Test de distribucion nula:
realizar un ordenamiento aleatorio de la secuencia en cuestión (ejemplo de la secuencia ASDFHAUOI, llevarla a una FASHDAOUI (contiene las mismas letras pero en otro orden) y calcular el score mediante mhci o mhcii en caso de las de 15 letras. Esto probarlo al menos 100 o 1000 veces, obtener así 1000 scores aleatorios y después se evaluará si el score observado está dentro o fuera de la distribución de los 1000 aleatorios. Con este test respondemos si el score observado pudo haberse obtenido con una secuencia aleatoria
 
Test de Mutaciones
El alfabeto de Aminoacidos tiene sólo las siguientes letras disponibles 
A R N D C Q E G H I L K M F P S T W Y V  (ver), entonces se deben generar mutaciones puntuales sobre la secuencia observada de una letra por vez de la secuencia y reemplazarla por alguna de las 19 letras (son 20 pero la que estoy reemplazando la no se puede repetir)y volver a estimar el score mediante la red. De esta manera se podrá obtener si generando alguna mutacion en la secuencia es posible obtener un peptido-epitipe candidato. Ejemplo( dada la secuencia ASDFHAUOI, primero me paro en la primera posicion y reemplazo “A” por R, luego N y asi hasta la V. Luego me paso a la segunda posicion y repito, para cada reemplazo cálculo el score).
 
Debe entregar un software que dada una secuencia de una proteina en formato fasta (identifique todos los peptidos candidatos y muestre en una tabla ordenados segun el score. Luego, seleccionar una secuencia de peptidos candidatos y poder realizar los dos test sugeridos.
 
Recursos disponibles:
deeptope MHC I and II.zip : deep CNN para predecir epitopes inmunogenicos. Heerramienta en PYTHON y Tensor Flow (ver inicio de este documento)
 
archivos: mhci.txt y mchii.txt, resultados de correr deeptope mhci y deeptope mhcii sobre la secuencia fasta SpikeWT.fasta que esta tambien en esta carpeta.
 
Leer bien el inicio de este documento para identificar las limitaciones que tiene la implementacion.
 
 
Las dudas las consultan con el profesor Elmer Fernandez.
 
---------------
La nueva version esta en el VersionDeepTope2.zip
 
Hey Elmer!
I changed the code and you can now run it as 
 
./src/deepitope.py -m MODEL -i FASTA -o OUTPUT_FILE_NAME
 
so 
./src/deepitope.py -m mhcii -i SpikeWT.fasta -o mhcii.out
will output the mhcii epitopes of the SpikeWT.fasta file into the mhcii.out file. 
 
The output file is optional, and if don't include it, 
./src/deepitope.py -m mhcii -i SpikeWT.fasta
it'll output to the terminal.
 
I also included a sample code for using deepitope as a python module (test_deepitope_as_module.py). The 'deepitope.predict' function returns a list in which each element is a tuple formatted as
(float(score), str(peptide), int(beginning position), int(final position)). It should be easy to parse it, but if you have any problems, please do not hesitate to contact me.
 
I prepared the code for some future additions I want to make, such as mapping epitopes of different sizes, and remove the indexing of the aminoacids. If you have any suggestions, I'd be happy to listen to them.
 
Best,
Raphael
